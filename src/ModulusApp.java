import java.util.*;

public class ModulusApp {
    public static void main(String[] args) {
        System.out.println("+------------------------------------------------+");
        System.out.println("|              Modulus Checker App               |");
        System.out.println("+------------------------------------------------+");
        System.out.print(" Masaukkan angka : ");

        Scanner inputAngka = new Scanner(System.in);
        int angka = inputAngka.nextInt();
        System.out.println("+------------------------------------------------+");
        System.out.println(" Bilangan " + angka + " Habis dibagi oleh bilangan berikut : ");
        for (int i = 1; i <= angka; i++) {
            if (angka % i == 0){
                System.out.println(" " + i);
            }
        }
        System.out.println("+------------------------------------------------+");
        inputAngka.close();
    }
}
